App.info({
    id: 'com.timewise.mobile',
    name: 'TimeWise',
    author: '23:Seconds',
    description: 'TimeWise app',
    version: '1.0.0',
    website: 'http://www.23seconds.be'
});

App.icons({
    // iOS
    'iphone_2x': 'resources/icons/ios/Icon-60@2x.png',
    'iphone_3x': 'resources/icons/ios/Icon-60@3x.png',
    'ipad': 'resources/icons/ios/Icon-76.png',
    'ipad_2x': 'resources/icons/ios/Icon-76@2x.png',

    // Android
    'android_mdpi': 'resources/icons/android/48.png',
    'android_hdpi': 'resources/icons/android/72.png',
    'android_xhdpi': 'resources/icons/android/96.png'
});

App.launchScreens({
    // iOS
    'iphone_2x': 'resources/splash/splash-screen.png',
    'iphone5': 'resources/splash/splash-screen.png',
    'iphone6': 'resources/splash/splash-screen.png',
    'iphone6p_portrait': 'resources/splash/splash-screen.png',
    'iphone6p_landscape': 'resources/splash/splash-screen.png',

    // Android
    'android_mdpi_portrait': 'resources/splash/splash-screen.png',
    'android_mdpi_landscape': 'resources/splash/splash-screen.png',
    'android_hdpi_portrait': 'resources/splash/splash-screen.png',
    'android_hdpi_landscape': 'resources/splash/splash-screen.png',
    'android_xhdpi_portrait': 'resources/splash/splash-screen.png',
    'android_xhdpi_landscape': 'resources/splash/splash-screen.png'
});

App.setPreference('Fullscreen', false);
App.setPreference('StatusBarOverlaysWebView', true);
App.setPreference('orientation', 'portrait');
App.setPreference('target-device', 'handset');
//App.setPreference('detect-data-types', 'false');
App.setPreference('android-windowSoftInputMode', 'adjustPan');
//App.setPreference('HideKeyboardFormAccessoryBar', true);
App.setPreference('KeyboardShrinksView', false);
App.setPreference('DisallowOverscroll', true);