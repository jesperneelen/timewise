interface Repository {
    _id? : string,
    name : string,
    fullName : string,
    description : string,
    language : string,
    repositoryUrl : string,
    createdAt : Date,
    updatedAt : Date, 
    branches : Array<string>,
    uuid : string
}