interface Commit {
    _id? : string,
    repoId : string,
    message : string,
    branch : string,
    author : string,
    avatarAuthor : string
}