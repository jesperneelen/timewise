interface Location {
    _id? : string,
    name : string,
    coord : { latitude : number, longitude : number},
    userId : string,
    createdAt : Date,
    updatedAt : Date
}