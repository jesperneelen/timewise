interface Issue {
    _id? : string,
    assignee : string,
    comments : Array<string>,
    description : string,
    id : string,
    summary : string,
    priority : string,
    projectId : string,
    reporter : string,
    sprintId : string,
    status : string,
    type : string,
    worklog : Array<Object>
}