interface Sprint {
    _id? : string,
    sprintId : string,
    name : string,
    completeDate? : Date,
    startDate : Date,
    endDate : Date,
    projectId : string,
    state : string
}