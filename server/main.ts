//REST API (for the background service location plugin)
import './imports/rest-api.ts';

//METEOR METHODS
import '../both/methods/users.methods';
import '../both/methods/clients.methods';
import '../both/methods/locations.methods';
import '../both/methods/projects.methods';

//METEOR PUBLICATIONS
import './imports/publications/users';
import './imports/publications/clients';
import './imports/publications/locations';
import './imports/publications/projects';
import './imports/publications/checkIns';

//INSERT TEST DATA AT STARTUP
import { Clients } from '../both/collections/clients.collection';
import { Projects } from '../both/collections/projects.collection';
import { Locations } from '../both/collections/locations.collection';

import { loadTestClients } from './imports/fixtures/clients';

Meteor.startup(() => {

    loadTestClients();

    if (Locations.find().cursor.count() === 0) {
        Locations.collection.insert({
            name: '23:Seconds HQ',
            coord: { latitude: 51.0376951, longitude: 4.4469551 },
            userId: '5J3wKEFojvmra2SDh',
            createdAt: new Date(),
            updatedAt: new Date()
        });
    }

    if (Projects.find().cursor.count() === 0) {
        Projects.collection.insert({
            name: 'iungoSport',
            key: 'IUNGO',
            clientId: 'BsHemSYhvmabisFFf',
            locationIds: [],
            disabled: false,
            timesheetCodes: [],
            userId: '5J3wKEFojvmra2SDh',
            createdAt: new Date(),
            updatedAt: new Date()
        });
    }
});