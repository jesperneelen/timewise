import { Meteor } from 'meteor/meteor';
import { Clients }  from '../../../both/collections/clients.collection';

Meteor.publish('myClients', function() {
    return Clients.collection.find({userId: this.userId}, {sort: { name : 1}});
});

Meteor.publish('singleClient', function(clientId: string) {
    return Clients.collection.find({_id: clientId, userId: this.userId});
});