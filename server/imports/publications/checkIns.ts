import { Meteor } from 'meteor/meteor';
import { CheckIns } from '../../../both/collections/checkins.collection';

Meteor.publish('getCurrentCheckIn', function() {
    return CheckIns.collection.find({userId: this.userId, 'time.end': { $exists: false }});
});

