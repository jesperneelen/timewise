import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

Meteor.publish('users', () => {
    return Meteor.users.find({}, {fields: { emails: 1, profile: 1, username: 1}});
});

Meteor.publish('getUser', function() {
    return Meteor.users.find({_id: this.userId}, {fields: { emails: 1, profile: 1, username: 1}});
});

Meteor.publish('singleUser', (userId) => {
    return Meteor.users.find({_id: userId}, {fields: {emails: 1, profile: 1, username: 1}});
});

Accounts.onCreateUser((options, user) => {
    user.profile = {
        birthday: options.profile.birthday,
        firstLogin: true,
        emailNotifications: false,
        phoneNotifications: false,
        photo: options.profile.photo,
        organisation: options.profile.organisation,
        jobtitle: options.profile.jobtitle
    };

    return user;
});
