import { Meteor } from 'meteor/meteor';
import { Projects }  from '../../../both/collections/projects.collection';

Meteor.publish('myProjects', function() {
    return Projects.collection.find({userId: this.userId}, {sort: { name : 1}});
});

Meteor.publish('singleProject', function(projectId: string) {
    return Projects.collection.find({_id: projectId, userId: this.userId})
});