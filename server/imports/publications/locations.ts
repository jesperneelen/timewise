import { Meteor } from 'meteor/meteor';
import { Locations }  from '../../../both/collections/locations.collection';

Meteor.publish('myLocations', function() {
    return Locations.collection.find({userId: this.userId}, {sort: { name : 1}});
});

Meteor.publish('singleLocation', function(locationId: string) {
    return Locations.collection.find({_id: locationId, userId: this.userId});
});