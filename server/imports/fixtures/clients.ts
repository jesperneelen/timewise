import {Clients} from'../../../both/collections/clients.collection';
import {Client} from'../../../both/models/client.model';

export function loadTestClients() {
    if (Clients.find().cursor.count() === 0) {
        Clients.collection.insert({
            name: 'iungoSport NV',
            userId: '5J3wKEFojvmra2SDh',
            createdAt: new Date(),
            updatedAt: new Date()
        });
    }
}