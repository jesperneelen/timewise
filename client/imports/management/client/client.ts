import 'angular2-meteor-polyfills';
import 'reflect-metadata';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MeteorReactive } from 'angular2-meteor';
import { Meteor } from 'meteor/meteor';

import template from './client.html';
import { Toolbar } from '../../toolbar/toolbar';
import { Clients } from '../../../../both/collections/clients.collection';

@Component({
    selector: 'management-client',
    template
})

export class ManagementClient extends MeteorReactive implements OnInit {
    clientId : string;
    client : Client;
    create : boolean;
    title : string;
    clientForm: FormGroup;
    clientFormSubmitted : boolean = false;

    constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder) {
        super();
    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            if(params['id'] === 'new') {
                this.create = true;
                this.title = 'Create Client';
                this.client = {
                    name: '',
                    userId: Meteor.userId(),
                    createdAt: new Date(),
                    updatedAt: new Date()
                };
            } else {
                this.create = false;
                this.title = 'Edit Client';
                this.clientId = params['id'];

                this.subscribe('singleClient', this.clientId, () => {
                    this.client = Clients.findOne(this.clientId);
                }, true);
            }
        });

        /*this.route.params
            .map(params => params['id'])
            .subscribe(id => {
                if(id === 'new') {
                    this.create = true;
                    this.title = 'Create Client';
                    this.client = {
                        name: '',
                        userId: Meteor.userId(),
                        createdAt: new Date(),
                        updatedAt: new Date()
                    };
                } else {
                    this.create = false;
                    this.title = 'Edit Client';
                    this.clientId = id;

                    this.subscribe('singleClient', this.clientId, () => {
                        this.client = Clients.findOne(this.clientId);
                    }, true);
                }
            });*/

        this.clientForm = this.fb.group({
            name: ['', Validators.required]
        });
    }

    deleteItem(eventData : any) {
        if(eventData.managementModule === 'clientManagement') {
            var confirmClientDialog = confirm('Are you sure you want to remove this client? It\'s possible ' +
				'that there are projects/locations linked to this client');

			if(confirmClientDialog) {
				Meteor.call('removeClient', eventData.itemId, (error, response) => {
                    if(error) toastr.error(error.reason);
                    else if(response === 1) {
                        toastr.success('Successfully removed this client');
                        this.router.navigate(['/management']);
                    } else if(response === 0) {
                        toastr.error('Something went wrong while deleting this client');
                    }
                });
			} else {
				toastr.error('Cancelled deleting of this client');
			}
        }
    }

    saveItem(eventData : any) {
        if(eventData.managementModule === 'clientManagement') {
            this.clientFormSubmitted = true;
            if (!this.clientForm.valid) {
			    toastr.error('Invalid form! Please resolve the errors below');
		    } else {
                if(this.create) {
                    Meteor.call('insertClient', this.client, (error, response) => {
                        if(error) toastr.error(error.reason);
                        else if(response) {
                            toastr.success('Client successfully created');
                            this.router.navigate(['/management']);
                        }
                    });
                } else {   
                    Meteor.call('updateClient', this.client.name, this.client._id, (error, response) => {
                        if(error) toastr.error(error.reason);
                        else if(response === 1) {
                            toastr.success('Client successfully updated');
                            this.router.navigate(['/management']);
                        } else {
                            toastr.error('Something went wrong while updating this client');
                        }
                    });
                }
            }
        }
    }
}