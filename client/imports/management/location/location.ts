import 'angular2-meteor-polyfills';
import 'reflect-metadata';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators,  } from '@angular/forms';
import { MeteorReactive } from 'angular2-meteor';
import { Meteor } from 'meteor/meteor';

import template from './location.html';
import { Toolbar } from '../../toolbar/toolbar';
import { Locations } from '../../../../both/collections/locations.collection';

@Component({
    selector: 'management-location',
    template
})

export class ManagementLocation extends MeteorReactive implements OnInit {
    locationId : string;
    location : Location;
    create : boolean;
    title : string;
    locationForm : FormGroup;
    locationFormSubmitted : boolean = false;

    constructor(private route: ActivatedRoute, private router: Router, private fb: FormBuilder) {
        super();
    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            if(params['id'] === 'new') {
                this.create = true;
                this.title = 'Create Location';
                this.location = {
                    name: '',
                    coord: {
                        longitude: '',
                        latitude: ''
                    },
                    userId: Meteor.userId(),
                    createdAt: new Date(),
                    updatedAt: new Date()
                };
            } else {
                this.create = false;
                this.title = 'Edit Location';
                this.locationId = params['id'];
        
                this.subscribe('singleLocation', this.locationId, () => {
                    this.location = Locations.findOne(this.locationId);
                }, true);
            }
        });
        /*this.route.params  
            .map(params => params['id'])
            .subscribe(id => {
                if(id === 'new') {
                    this.create = true;
                    this.title = 'Create Location';
                    this.location = {
                        name: '',
                        coord: {
                            longitude: '',
                            latitude: ''
                        },
                        userId: Meteor.userId(),
                        createdAt: new Date(),
                        updatedAt: new Date()
                    };
                } else {
                    this.create = false;
                    this.title = 'Edit Location';
                    this.locationId = id;
            
                    this.subscribe('singleLocation', this.locationId, () => {
                        this.location = Locations.findOne(this.locationId);
                    }, true);
                }
            });*/

        this.locationForm = this.fb.group({
            name: ['', Validators.required],
            latitude: ['', Validators.required],
            longitude: ['', Validators.required]
        });
    }

    deleteItem(eventData: any) {
        if(eventData.managementModule === 'locationManagement') {
            var confirmLocationDialog = confirm('Are you sure you want to remove this location? It\'s possible ' +
				'that there are projects linked to this location');

			if(confirmLocationDialog) {
				Meteor.call('removeLocation', eventData.itemId, (error, response) => {
                    if(error) toastr.error(error.reason);
                    else if(response === 1) {
                        toastr.success('Successfully removed this location');
                        this.router.navigate(['/management']);
                    } else if(response === 0) {
                        toastr.error('Something went wrong while deleting this location');
                    }
                });
			} else {
				toastr.error('Cancelled deleting of this location');
			}
        }
    }
    
    saveItem(eventData: any) {
        if(eventData.managementModule === 'locationManagement') {
            this.locationFormSubmitted = true;
            if (!this.locationForm.valid) {
			    toastr.error('Invalid form! Please resolve the errors below');
		    } else {
                if(this.create) {
                    Meteor.call('insertLocation', this.location, (error, response) => {
                        if(error) toastr.error(error.reason);
                        else if(response) {
                            toastr.success('Location successfully created');
                            this.router.navigate(['/management']);
                        }
                    });
                } else {   
                    Meteor.call('updateLocation', this.location._id, this.location.name, this.location.coord.latitude, this.location.coord.longitude,
                        (error, response) => {
                            if(error) toastr.error(error.reason);
                            else if(response === 1) {
                                toastr.success('Location successfully updated');
                                this.router.navigate(['/management']);
                            } else {
                                toastr.error('Something went wrong while updating this location');
                            }
                        });
                }
            }
        }
    }
}

/*
    //get current location
    geolocation.getLocation().then(function (data) {
        self.cord.lat = data.coords.latitude;
        self.cord.lon = data.coords.longitude;
    });
*/