import 'angular2-meteor-polyfills';
import 'reflect-metadata';
import { Component, OnInit } from '@angular/core';
import { MeteorReactive } from 'angular2-meteor';
import { Mongo } from 'meteor/mongo';

import template from './management.html';
import { Toolbar } from '../toolbar/toolbar';

import { Clients } from '../../../both/collections/clients.collection';
import { Locations } from '../../../both/collections/locations.collection';
import { Projects } from '../../../both/collections/projects.collection';

@Component({
    selector: 'management',
    template
})

export class Management extends MeteorReactive implements OnInit {
    clients : Mongo.Cursor<Client>;
    projects : Mongo.Cursor<Project>;
    locations : Mongo.Cursor<Location>;
    banners : Array<string> = [];
    selectedTab : string = 'client';
    makeNew : string = 'client';

    constructor() {
        super();
    }

    ngOnInit() {
        this.subscribe('myClients', () => {
            this.clients = Clients.find();
        });

        this.subscribe('myProjects', () => {
            this.projects = Projects.find();
        });

        this.subscribe('myLocations', () => {
            this.locations = Locations.find();
        });
    }

    // sets the current list (project, client, or location) to create a new one from the right type
    setSelectedTab(item) {
        this.makeNew = item;
        this.selectedTab = item;

        if (item === 'client') {
            //$rootScope.$broadcast('add-view');
        } else if (item === 'project') {
            //$rootScope.$broadcast('project-view');
        } else if (item === 'location') {
            //$rootScope.$broadcast('add-view');
        }
    }
}


//         $scope.setNew = function (item) {
//             $scope.makeNew = item;
//             if (item == 'client') {
//                 $rootScope.$broadcast('add-view');
//             }
//             else if (item == 'project') {
//                 $rootScope.$broadcast('project-view');
//             }
//             else if (item == 'location') {
//                 $rootScope.$broadcast('add-view');
//             }
//         };