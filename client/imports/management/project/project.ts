import 'angular2-meteor-polyfills';
import 'reflect-metadata';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MeteorReactive } from 'angular2-meteor';
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

import template from './project.html';
import { Toolbar } from '../../toolbar/toolbar';
import { Projects } from '../../../../both/collections/projects.collection';
import { Locations } from '../../../../both/collections/locations.collection';
import { Clients } from '../../../../both/collections/clients.collection';

@Component({
    selector: 'management-project',
    template
})

export class ManagementProject extends MeteorReactive implements OnInit {
    project : Project;
    projectId : string;
    projectForm : FormGroup;
    projectFormSubmitted : boolean = false;
    timesheetCodeForm : FormGroup;
    create : boolean;
    title : string;
    clients : Mongo.Cursor<Client>;
    locations : Mongo.Cursor<Location>;
    isCodeEditing : boolean = false;
    indexCodeEditing : number;
    code : string;

    constructor(private route : ActivatedRoute, private router : Router, private fb : FormBuilder) {
        super();
    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            if(params['id'] === 'new') {
                this.create = true;
                this.title = 'Create Project';
                this.project = {
                    name: '',
                    key: '',
                    clientId: '',
                    locationIds: [],
                    timesheetCodes: [],
                    disabled: false,
                    userId: Meteor.userId(),
                    createdAt: new Date(),
                    updatedAt: new Date()
                };
            } else {
                this.create = false;
                this.title = 'Edit Project';
                this.projectId = params['id'];
        
                this.subscribe('singleProject', this.projectId, () => {
                    this.project = Projects.findOne(this.projectId);
                }, true);
            }
        });
        /*this.route.params  
            .map(params => params['id'])
            .subscribe(id => {
                if(id === 'new') {
                    this.create = true;
                    this.title = 'Create Project';
                    this.project = {
                        name: '',
                        key: '',
                        clientId: '',
                        locationIds: [],
                        timesheetCodes: [],
                        disabled: false,
                        userId: Meteor.userId(),
                        createdAt: new Date(),
                        updatedAt: new Date()
                    };
                } else {
                    this.create = false;
                    this.title = 'Edit Project';
                    this.projectId = id;
            
                    this.subscribe('singleProject', this.projectId, () => {
                        this.project = Projects.findOne(this.projectId);
                    }, true);
                }
            });*/

        this.subscribe('myClients', () => {
            this.clients = Clients.find({}, {sort: {name: 1}});
        });

        this.subscribe('myLocations', () => {
            this.locations = Locations.find({}, {sort: {name: 1}});
        });

        this.timesheetCodeForm = this.fb.group({
            code: ['', Validators.required]
        });

        this.projectForm = this.fb.group({
            name: ['', Validators.required],
            clientId: ['', Validators.required],
            key: ['', Validators.required],
            disabled: false
        });
    }

    deleteItem(eventData: any) {
        if(eventData.managementModule === 'projectManagement') {
            var confirmProjectDialog = confirm('Are you sure you want to remove this project?');

			if(confirmProjectDialog) {
				Meteor.call('removeProject', eventData.itemId, (error, response) => {
                    if(error) toastr.error(error.reason);
                    else if(response === 1) {
                        toastr.success('Successfully removed this project');
                        this.router.navigate(['/management']);
                    } else if(response === 0) {
                        toastr.error('Something went wrong while deleting this project');
                    }
                });
			} else {
				toastr.error('Cancelled deleting of this project');
			}
        }
    }

    saveItem(eventData: any) {
        if(eventData.managementModule === 'projectManagement') {
            this.projectFormSubmitted = true;
            console.log(this.projectForm);
            console.log(this.project);

            if(this.project.locationIds.length === 0) {
                toastr.error('Choose at least 1 location');
            } else if(this.project.timesheetCodes.length === 0) {
                toastr.error('Add at least 1 timesheet code');
            } else if(this.projectForm.valid) {
                if(this.create) {
                    Meteor.call('insertProject', this.project, (error, response) => {
                        if(error) toastr.error(error.reason);
                        else if(response) {
                            toastr.success('Project successfully created');
                            this.router.navigate(['/management']);
                        }
                    });
                } else {   
                    Meteor.call('updateProject', this.project._id, this.project.name, this.project.key, this.project.disabled, this.project.clientId, this.project.locationIds, this.project.timesheetCodes, 
                        (error, response) => {
                            if(error) toastr.error(error.reason);
                            else if(response === 1) {
                                toastr.success('Project successfully updated');
                                this.router.navigate(['/management']);
                            } else {
                                toastr.error('Something went wrong while updating this project');
                            }
                        });
                }
            } else {
                toastr.error('Invalid form! Please resolve the errors below');
            }
        }
    }

    addTimesheetCode() {
        if(this.timesheetCodeForm.valid) {
            if(this.project.timesheetCodes.indexOf(this.timesheetCodeForm.controls['code'].value) === -1) {
                this.project.timesheetCodes.push(this.timesheetCodeForm.controls['code'].value.trim());
                this.code = '';
            }
        }
    }

    removeTimesheetCode(index: number) {
        this.project.timesheetCodes.splice(index, 1);
        this.isCodeEditing = false;
        this.indexCodeEditing = undefined;
    }

    editTimesheetCode(index: number) {
        this.isCodeEditing = true;
        this.indexCodeEditing = index;
        this.code = this.project.timesheetCodes[index];
    }

    saveTimesheetCode() {
        if(this.timesheetCodeForm.valid) {
            this.project.timesheetCodes[this.indexCodeEditing] = this.timesheetCodeForm.controls['code'].value;
            this.isCodeEditing = false;
            this.code = '';
        }
    }
}