import 'angular2-meteor-polyfills';
import 'reflect-metadata';
import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Meteor } from 'meteor/meteor';
import { MeteorReactive } from 'angular2-meteor';
import { InjectUser } from 'angular2-meteor-accounts-ui';

import template from './toolbar.html';

@Component({
    selector: 'toolbar',
    template
})

@InjectUser('user')

export class Toolbar extends MeteorReactive implements OnInit {
    isCollapsed : boolean = true;
    locationMonth : boolean = false;
    locationSettings : boolean = false;
    hideToggle : boolean = false;
    showClose : boolean = false;
    showPrev : boolean = false;
    showAdd : boolean = false;
	showDelete : boolean = false;
    showPhotoUrl : boolean = false;
    user : Meteor.User;
    dataURL : string;
    photoUrl : string;
	management : boolean;
	managementModule : string;

    @Input('title') title : string;
    @Input('bg') bg : string;
    @Input('close') close : string;
    @Input('back') back : string;
    @Input('add') add : string;
	@Input('remove') remove: string;
    @Input('face') face : string;
	@Input('settings') settings : string;

	@Output('deleteItem') deleteItem = new EventEmitter();
	@Output('saveItem') saveItem = new EventEmitter();

    constructor(private router : Router) {
        super();
        console.log(this.user);    
    }

    ngOnInit() {
        if (this.close === '/') {
			this.hideToggle = true;
			this.showClose = true;
		}

		if (this.close) {
			this.hideToggle = true;
			this.showClose = true;
		} else if (this.back) {
			this.hideToggle = true;
			this.showPrev = true;
		}

		//Show the Plus/add button in the management modules
        if(this.add) this.showAdd = true;

        if (this.user && this.user.profile.photo) {
			this.dataURL = this.user.profile.photo;
		}

		if (!this.back && this.user && this.face === 'true') {
			this.showPhotoUrl = true;
			this.photoUrl = this.dataURL;
		} else {
			this.showPhotoUrl = false;
		}

		if (this.settings === 'projectManagement' || this.settings === 'clientManagement' || this.settings === 'locationManagement') {
			this.management = true;
			this.managementModule = this.settings;
		}

		// Show Delete button in management modules
		if(this.remove) this.showDelete = true;
    }

	//Log out of the Meteor accounts system
    LogOut() {
		Meteor.logout((error) => {
			if (error) console.log(error.reason);
			else this.router.navigate(['/']);
		});
	}

	//navigate to the different management components (/management/project/:id, /management/location/:id, /management/client/:id)
	makeNew() {
		if (this.add) this.router.navigate(['/management/' + this.add + '/new']);
	}

	//Let the management modules know that the user clicked on the garbage bin
	deleteManagement() {
		this.deleteItem.emit({managementModule: this.managementModule, itemId: this.remove});
	}

	//Let the management modules know that the user clicked on the save icon
	saveManagement() {
		this.saveItem.emit({managementModule: this.managementModule});
	}
}

/* 
	this._SettingsFactory = SettingsFactory;

	if (this.month === 'selectTitle') this.locationMonth = true;
	if (this.settings === 'personalSettings') this.locationSettings = true;

	
	this.calendarOptions = [
		{id: 1, name: 'JANUARY'}, {id: 2, name: 'FEBRUARY'}, {id: 3, name: 'MARCH'}, {id: 4, name: 'APRIL'},
		{id: 5, name: 'MAY'}, {id: 6, name: 'JUNE'}, {id: 7, name: 'JULY'}, {id: 8, name: 'AUGUST'},
		{id: 9, name: 'SEPTEMBER'}, {id: 10, name: 'OCTOBER'}, {id: 11, name: 'NOVEMBER'}, {
			id: 12,
			name: 'DECEMBER'
		}];

	
	saveSettings() {
		this._SettingsFactory.setSaveSettings();
	}

	takePhoto() {
		this._SettingsFactory.setTakePhoto();
	}
}

export default angular.module(name, [angularMeteor, SettingsFactory])
	.component(name, {
		templateUrl,
		controllerAs: name,
		controller: Toolbar,
		bindings: {
			search: '@search',
			view: '@view',
			month: '@month',
		}
	});

//					/**
//					 * De huidige maand als standaard zetten in de dropdownlist in de toolbar
//					 */
//					$scope.$on('beginDropdown', function (event, data) {
//						var ThreeLetterMonth = data.split('-');
//						for (var i = 0; i < calendarOptions.length; i++) {
//							if (ThreeLetterMonth[1].toLowerCase() === calendarOptions[i].name.substring(0, 3).toLowerCase()) {
//								$scope.selectedOption = calendarOptions[i].id;
//							}
//						}
//					});
//
//					/**
//					 * Kijken of er een andere maand wordt geselecteerd in de month-view en deze maand dan doorgeven
//					 * naar de datetimepicker.js waar de geselecteerde maand zal worden gerendered.
//					 */
//					$scope.onChange = function (selectedOption) {
//						calendarFactory.setSelectedMonth(selectedOption, new Date().getFullYear());
//					};
//
//					$scope.saveManagement = function () {
//						managementFactory.setSaveManagement(true, $scope.managementModule);
//					};
//				}
//			};
//		}]);

/*
 <toolbar></toolbar> normal functionality, no title
 <toolbar title="Check in"></toolbar> normal with title
 <toolbar back="URL"></toolbar> backbutton instead of menu, title possible
 <toolbar close="URL"></toolbar> close button instead of menu, title possible
 <toolbar title="Check in" bg="color"></toolbar> custom bgcolor, always possible
 <toolbar add="client/project/loaction"></toolbar> navigates to correct view
 <toolbar remove="_id"></toolbar> removes client or location
 */