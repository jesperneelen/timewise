import 'angular2-meteor-polyfills';
import 'reflect-metadata';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

import template from './login.html';

@Component({
    selector: 'login',
    template
})

export class Login implements OnInit {
    signInForm : FormGroup;
    requestNewPasswordForm : FormGroup;
	requestNewPassword : boolean = false;
    signInSubmitted : boolean = false;
    requestNewPasswordSubmitted : boolean = false;

    constructor(private formBuilder : FormBuilder, private router : Router) {}

    ngOnInit() {
        this.signInForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        this.requestNewPasswordForm = this.formBuilder.group({
            email: ['', Validators.required]
        });
    }

    logIn() {
        this.signInSubmitted = true;
    
        if (!this.signInForm.valid) {
			toastr.error('Invalid form! Please resolve the errors below');
		} else {
			Meteor.loginWithPassword(this.signInForm.value.username, this.signInForm.value.password, (error) => {
				if (error) {
					toastr.error(error.reason);
				} else {
					toastr.success('Successfully logged in');

					this.router.navigate(['/home']);
				}
			});
		}
    }

    requestnewPass() {
        this.requestNewPasswordSubmitted = true;
		
		if (!this.requestNewPasswordForm.valid) {
			toastr.error('Invalid form! Please resolve the errors below');
		}
		else {
			Accounts.forgotPassword({email: this.requestNewPasswordForm.value.email}, (error) => {
				if (error) toastr.error(error.reason);
				else toastr.success('Email has been sent. Please check your email for further instructions')
			});
		}
	}
}

/*
export default angular.module(name, [angularMeteor, uiRouter, ToastFactory])
	.component(name, {
		templateUrl,
		controller: Login,
		controllerAs: name
	})
	.config(($stateProvider) => {
		$stateProvider
			.state('login', {
				url: '/',
				template: '<login></login>',
				resolve: {
					user($location) {
						if(Meteor.userId()){
							Meteor.subscribe('getUser', Meteor.userId(), function(){
								let user = Meteor.users.findOne({_id: Meteor.userId()});

								if(user.profile.firstLogin) {
									$location.path('/walkThrough');
								} else {
									$location.path('/checkIn');
								}
							});
						}
					}
				}
			});
	});
*/