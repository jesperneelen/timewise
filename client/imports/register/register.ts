import 'angular2-meteor-polyfills';
import 'reflect-metadata';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MeteorCamera } from 'meteor/mdg:camera';
import { MeteorReactive } from 'angular2-meteor';
import { Session } from 'meteor/session';
import { Toolbar } from '../toolbar/toolbar';

import template from './register.html';

function passwordValidator(control: FormControl): { [s: string]: boolean } {
	if (!control.value.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}/)) {
		return { InvalidPassword : true };
	}
}

function emailValidator(control: FormControl): { [s: string]: boolean } {
	if(!control.value.match(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/)) {
		return { InvalidEmail : true };
	}
}

@Component({
    selector: 'register',
    template
})

export class Register extends MeteorReactive implements OnInit {
    signUpForm: FormGroup;
	showAddPictureButton: boolean = true;
	signUpFormSubmitted: boolean = false;
	url: any;

    constructor(private formBuilder: FormBuilder, private router: Router) {
		super();
	}

    ngOnInit() {
        this.signUpForm = this.formBuilder.group({
            name: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(35)])],
            email: ['', Validators.compose([Validators.required, emailValidator])],
            password: ['', Validators.compose([Validators.required, passwordValidator])],
            confirmPassword: ['', Validators.compose([Validators.required, passwordValidator])],
            organisation: ['', Validators.required],
            jobtitle: ['', Validators.required],
            birthday: ['']
        });

		this.autorun(() => {
			this.url = Session.get('dataURL');
			if (this.url) this.showAddPictureButton = false;
		});
    }

    signUp() {
		this.signUpFormSubmitted = true;

		if (!this.url) {
			toastr.error('First take a profile picture!');
		} else if(this.signUpForm.controls.email.errors && this.signUpForm.controls.email.errors.InvalidEmail) {
			toastr.error('Invalid email address');
		} else if(this.signUpForm.controls.password.errors && this.signUpForm.controls.password.errors.InvalidPassword) {
			toastr.error('Password must be at least 8 characters long (max. 20), must contain at least one lower and one uppercase letter and one non-alpha character');
		} else if (this.signUpForm.value.password !== this.signUpForm.value.confirmPassword) {
			toastr.error('Passwords don\'t match');
		} else if (this.signUpForm.valid) {
			Accounts.createUser({
				username: this.signUpForm.value.name,
				email: this.signUpForm.value.email,
				password: this.signUpForm.value.password,
				profile : {
					birthday: this.signUpForm.value.birthday,
					photo: this.url,
					jobtitle: this.signUpForm.value.jobtitle,
					organisation: this.signUpForm.value.organisation
				}
			}, (error) => {
				if (error) {
					toastr.error(error.reason);
				} else {
					toastr.success('Successfully registered!');
					this.router.navigate(['/home']);
					//Meteor.call('addUserToRole', this.signUpForm.value.name, Meteor.userId());			
					//this._$location.path('/walkThrough');
				}
			});
		} else {
			toastr.error('Invalid form! Please resolve the errors below');
		}
	}


	addPhoto() {
		MeteorCamera.getPicture({ correctOrientation: "portrait" }, (error, data) => {
			if (error) toastr.error(error.reason);
			else {
				this.showAddPictureButton = false;
				Session.set('dataURL', data);
			}
		});
	}
}

/*
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';

import { name as Toolbar } from '../toolbar/toolbar';
import { name as ToastFactory } from '../../factories/toastFactory.js';

class Register {
	constructor($location, ToastFactory) {
	
		this._$location = $location;
		this._ToastFactory = ToastFactory;
	
	}
}
export default angular.module(name, [angularMeteor, uiRouter, Toolbar, ToastFactory])
	.component(name, {
		templateUrl,
		controllerAs: name,
		controller: Register
	})
	.config(($stateProvider) => {
		'ngInject';

		$stateProvider
			.state('register', {
				url: '/register',
				template: '<register></register>',
				resolve : {
					loggedIn($location) {
						if (Meteor.userId()) $location.path('/checkin');
					}
				}
			});
	});

*/