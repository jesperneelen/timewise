import 'angular2-meteor-polyfills';
import 'reflect-metadata';
import { Component, OnInit } from '@angular/core';
import { MeteorReactive } from 'angular2-meteor';
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

import template from './home.html';
import { Toolbar } from '../toolbar/toolbar';
import { CheckIns } from '../../../both/collections/checkins.collection';
import { Projects } from '../../../both/collections/projects.collection';
import { Locations } from '../../../both/collections/locations.collection';

@Component({
    selector: 'home',
    template
})

export class Home extends MeteorReactive implements OnInit {
    today : Date = new Date();
    hour : number = this.today.getHours();
    checkIn : CheckIn;
    greeting : string;
    user : Meteor.User;
    userFirstname : string;
    projects : Mongo.Cursor<Project>;
    locations : Mongo.Cursor<Location>;

    constructor() {
        super();
        
        if (this.hour < 5 || this.hour >= 21) {
            this.greeting = "Good Night"
        } else if (this.hour < 11) {
            this.greeting = "Good morning";
        } else if (this.hour < 13) {
            this.greeting = "Greetings";
        } else if (this.hour < 17) {
            this.greeting = "Good afternoon";
        } else {
            this.greeting = "Good evening";
        }
    }

    ngOnInit() {
        this.subscribe('getCurrentCheckIn', () => {
            this.checkIn = CheckIns.findOne({userId: Meteor.userId(), 'time.end': { $exists: false }});
            console.log(this.checkIn);

            if(!this.checkIn) {
                console.log('no current checkin');
            }
        });

        this.subscribe('getUser', () => {
            this.user = Meteor.users.findOne({_id: Meteor.userId()});
            this.userFirstname = this.user.username.split(' ')[0];
        });

        this.subscribe('myProjects', () => {
            this.projects = Projects.find({userId: Meteor.userId()});
        });

        this.subscribe('myLocations', () => {
            this.locations = Locations.find({userId: Meteor.userId()});
        });
    }
}


/*
angular.module('timeAppApp')
    .controller('CheckinCtrl', ['$scope', '$window', '$meteor', '$rootScope', '$interval', 'geolocation', 'uiGmapGoogleMapApi',
        function ($scope, $window, $meteor, $rootScope, $interval, geolocation, uiGmapGoogleMapApi) {
            var checkin = this;
            checkin.tryingCheckIn = true;
            var lastmins;
            this.count = "Loading...";
            

            //calculates distance between two points in km's
            function calcDistance(p1, p2) {
                return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2));
            }

            this.checkGeolocation = function () {
                checkin.tryingGeolocation = true;
                try {
                    geolocation.getLocation().then(function (data) {
                        var locationsForGeoLocation = Locations.find().fetch();
                        var localLatitude = data.coords.latitude;
                        var localLongitude = data.coords.longitude;

                        //data.coords.accuracy
                        uiGmapGoogleMapApi.then(function (maps) {
                            for (var i = 0; i < locationsForGeoLocation.length; i++) {
                                var coordsLocation = locationsForGeoLocation[i].cord;
                                var coordLocationLatitude = coordsLocation.split(',')[0];
                                var coordLocationLongitude = coordsLocation.split(',')[1];

                                var p1 = new google.maps.LatLng(localLatitude, localLongitude);
                                var p2 = new google.maps.LatLng(coordLocationLatitude, coordLocationLongitude);

                                if (calcDistance(p1, p2) < (data.coords.accuracy * 2)) {
                                    checkin.tryingGeolocation = false;
                                    checkin.geolocationSuccess = locationsForGeoLocation[i].name;
                                    checkin.selectedLocation = locationsForGeoLocation[i]._id;
                                    checkin.locationSelected();
                                    return;
                                }
                            }
                            checkin.tryingGeolocation = false;
                        });
                    }, function (reason) {
                        Notifications.error('ERROR', reason, {timeout: 3000});
                        checkin.tryingGeolocation = false;
                    });
                }
                catch (err) {
                    Notifications.error('ERROR', err, {timeout: 3000});
                }
            };

            Meteor.call('getCurrentCheckin', this.userID, function (error, result) {
                if (error || result == undefined || checkin.workingTooLong(result))
                    checkin.checkGeolocation();
                else
                    checkin.checkIn = result;
                checkin.tryingCheckIn = false;
                $scope.$apply();
            });

            this.workingTooLong = function (tempcheckIn) {
                var now = new Date();
                var diff = now.getTime() - tempcheckIn.time.start.getTime();
                var hours = Math.floor(diff / (1000 * 60 * 60));
                if (hours >= 8) {
                    var endtime = new Date(tempcheckIn.time.start).setHours(tempcheckIn.time.start.getHours() + 8);
                    tempcheckIn.time.end = new Date(endtime);

                    Meteor.call('endCheckin', checkin.userID, tempcheckIn, function (err, result) {
                        if (err)
                            Notifications.error("Error stopping timer", "Your last timing lasted longer than 8 hours, but the timer could not be stopped.", {timeout: 2500});
                        if (result) {
                            Notifications.warn("Timing stopped", "Your last timing lasted longer than 8 hours, it has automatically been stopped.", {timeout: 2500});
                            checkin.checkIn = undefined;
                            checkin.count = "Loading...";
                            checkin.location = "";
                            checkin.tryingCheckIn = false;
                            $scope.$apply();
                        }
                    });
                    return true
                }
                return false;
            };

            this.locationSelected = function () {
                checkin.tryingCheckIn = true;
                var location = Locations.find(checkin.selectedLocation).fetch()[0];
                if (Object.prototype.toString.call(location.project) == "[object Array]") {
                    Meteor.call('getLocationProjects', location.project, function (err, result) {
                        checkin.tempLocation = location;
                        checkin.multiprojectlist = result;
                        checkin.getProject = true;
                        $scope.$apply();
                    });
                }
            };

            this.projectSelected = function () {
                location = checkin.tempLocation;
                checkin.tempLocation = undefined;
                checkin.getProject = false;
                location.project = checkin.selectedProject;

                Meteor.call('getTimesheetCodesProjects', checkin.selectedProject, function(error, result) {
                    if(result.ts){
                        if (result.ts.multiple) {
                            checkin.getTimesheetcodes = true;
                            checkin.multiTimesheetCode = result.ts.advanced;
                            $scope.$apply();
                        } else {
                            checkin.doCheckIn(location, result.ts.code, false);
                        }
                    } else {
                        checkin.doCheckIn(location, undefined, false);
                    }
                });
            };

            this.timesheetCodeSelected = function(){
                var timesheetCode = checkin.selectedTimesheetcode;
                this.count = "00:00";
                checkin.getTimesheetcodes = false;
                checkin.doCheckIn(location, timesheetCode, false);
            };

        
            this.doCheckIn = function (location, timesheetCode) {
                checkin.selectedLocation = undefined;
                checkin.selectedProject = undefined;
                checkin.selectedTimesheetcode = undefined;

                if (!location) {
                    console.log("Error: Location undefined")
                }
                else {
                    this.checkIn = {
                        success: true,
                        user: checkin.userID,
                        location: location,
                        timesheetCode: timesheetCode,
                        time: {
                            start: new Date(),
                            end: undefined
                        }
                    };
                    Meteor.call('setCurrentCheckin', checkin.userID, checkin.checkIn, function (error, result) {
                        if (error || result == undefined) {
                            checkin.tryingCheckIn = false;
                            checkin.checkIn.success = false;
                            checkin.checkout();
                        }
                        else {
                            checkin.tryingCheckIn = false;
                            checkin.checkIn.success = true;
                        }
                    });

                    checkin.tryingCheckIn = false;
                    checkin.checkIn.success = true;
                }
                checkin.tryingCheckIn = false;
            };


            this.updateTime = $interval(function () {
                if (checkin.checkIn != undefined) {
                    var now = new Date();
                    var diff = now.getTime() - checkin.checkIn.time.start.getTime();
                    var hours = Math.floor(diff / (1000 * 60 * 60));
                    var mins = Math.floor(diff / (1000 * 60)) - (60 * hours);
                    if (hours >= 8) {
                        checkin.checkout();
                        Notifications.warn("Timing stopped", "You were checked in for 8 hours, the timer has automatically been stopped.", {timeout: 2500});
                    }
                    if (lastmins != mins) {
                        checkin.count = ("0" + hours).slice(-2) + ":" + ("0" + mins).slice(-2);
                    }
                    lastmins = mins;
                }
            }, 1000);


            this.checkout = function () {
                checkin.checkedIn = false;
                checkin.checkIn.time.end = new Date();
                Meteor.call('endCheckin', checkin.userID, checkin.checkIn, function (err, result) {
                    if (err) {
                        Notifications.error("Error stopping timer", "The timer could not be stopped.", {timeout: 2500});
                    }
                    if (result) {
                        checkin.checkIn = undefined;
                        checkin.count = "Loading...";
                        checkin.location = "";
                        $scope.$apply();
                    }
                });
            };

            $scope.$on('$stateChangeStart', function (next, current) {
                $interval.cancel(checkin.updateTime);
            });

            $rootScope.$watch('currentUser', function (newValue) {
                if (newValue) {
                    if (newValue._id) {
                        var name = $rootScope.currentUser.username;
                        name = name.split(" ");
                        checkin.name= name[0]; //only show firstname
                    }
                }
            });
        }]);
*/