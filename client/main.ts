import 'angular2-meteor-polyfills';
import 'reflect-metadata';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';

import { AppModule } from './app.module';

//METEOR METHODS
import '../both/methods/users.methods';
import '../both/methods/clients.methods';
import '../both/methods/locations.methods';
import '../both/methods/projects.methods';

enableProdMode();
platformBrowserDynamic().bootstrapModule(AppModule);
