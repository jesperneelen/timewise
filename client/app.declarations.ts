import { Toolbar } from './imports/toolbar/toolbar';
import { Login } from './imports/login/login';
import { Register } from './imports/register/register';
import { Home } from './imports/home/home';
import { Management } from './imports/management/management';
import { ManagementClient } from './imports/management/client/client';
import { ManagementLocation } from './imports/management/location/location';
import { ManagementProject } from './imports/management/project/project';
import { ChecklistDirective } from 'ng2-checklist';

export const COMPONENTS_DECLARATIONS = [
    Login, 
    Register, 
    Home, 
    Management, 
    ManagementClient, 
    ManagementLocation, 
    ManagementProject, 
    Toolbar, 
    ChecklistDirective
];
