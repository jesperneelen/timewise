import 'angular2-meteor-polyfills';
import 'reflect-metadata';
import { Component } from '@angular/core';

import template from './app.html';

@Component({
    selector: 'TimeWise',
    template
})

export class TimeWise {
    constructor() {
        if(Meteor.isCordova) {
            // This callback will be executed every time a geolocation is recorded in the background
            let successFn = (location) => {
                console.log('BackgroundGeolocation callback:  ' + location.latitude + ',' + location.longitude);
                backgroundGeolocation.finish();
            };
    
            // This callback will be executed every time an error was thrown by the location plugin
            let failureFn = (error) => {
                console.log('BackgroundGeolocation error' + JSON.stringify(error));
            };

            //Initialise the background/foreground location plugin
            // --> url & httpHeaders will point to the REST API set up (server/imports/rest-api.js)
            // --> every time a new location is recorded, this location will be send to that API
            backgroundGeolocation.configure(successFn, failureFn, {
                desiredAccuracy: 0,
                stationaryRadius: 10,
                distanceFilter: 10,
                startOnBoot: true, // Start background service on device boot. (Android)
                stopOnTerminate: false, // Enable this to clear background location settings when the app terminates
                url: 'http://localhost:3000/api/userLocation',
                httpHeaders: { 'x-user-id': Meteor.userId() ? Meteor.userId() : ''},
                saveBatteryOnBackground: false,
                //ANDROID ONLY SECTION
                interval: 5000,
                fastestInterval: 3000,
            });

            backgroundGeolocation.start();

            backgroundGeolocation.watchLocationMode((enabled) => {
                if (enabled) {
                    // location service are now enabled
                    // call backgroundGeolocation.start
                    backgroundGeolocation.start();
                } else {
                    if (error.code === 2) {
                        if (window.confirm('Not authorized for location updates. Would you like to open app settings?')) {
                            backgroundGeolocation.showAppSettings();
                        }
                    }
                }
            }, (error) => {
                console.log('Error watching location mode. Error:' + error);
            });

            backgroundGeolocation.isLocationEnabled((enabled) => {
                if (enabled) {
                    backgroundGeolocation.start(
                        () => {
                            // service started successfully
                            // you should adjust your app UI for example change switch element to indicate
                            // that service is running
                        },(error) => {
                            // Tracking has not started because of error
                            // you should adjust your app UI for example change switch element to indicate
                            // that service is not running
                            if (error.code === 2) {
                                if (window.confirm('Not authorized for location updates. Would you like to open app settings?')) {
                                    backgroundGeolocation.showAppSettings();
                                }
                            } else {
                                window.alert('Start failed: ' + error.message);  
                            }
                        }
                    );
                }
            });
        }
    }
}