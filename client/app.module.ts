import 'angular2-meteor-polyfills';
import 'reflect-metadata';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { METEOR_PROVIDERS } from 'angular2-meteor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { TimeWise } from './app.component';
import { COMPONENTS_DECLARATIONS } from './app.declarations';
import { routes } from './app.routes';

import { CanActivateWhenLoggedIn } from './auth-guard';

@NgModule({
  // Components, Pipes, Directive
  declarations: [
    TimeWise, 
    ...COMPONENTS_DECLARATIONS
  ],
  // Entry Components
  entryComponents: [
    TimeWise
  ],
  // Providers
  providers: [
    METEOR_PROVIDERS,
    CanActivateWhenLoggedIn
  ],
  // Modules
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    RouterModule, 
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  // Main Component
  bootstrap: [ TimeWise ]
})

export class AppModule {}