import { Route } from '@angular/router';

import { Toolbar } from './imports/toolbar/toolbar';
import { Login } from './imports/login/login';
import { Register } from './imports/register/register';
import { Home } from './imports/home/home';
import { Management } from './imports/management/management';
import { ManagementClient } from './imports/management/client/client';
import { ManagementLocation } from './imports/management/location/location';
import { ManagementProject } from './imports/management/project/project';

import { CanActivateWhenLoggedIn } from './auth-guard';

export const routes: Route[] = [
    { path: '', component: Login },
    { path: 'register', component: Register },
    { path: 'home', component: Home, canActivate: [CanActivateWhenLoggedIn] },
    { path: 'management', component: Management, canActivate: [CanActivateWhenLoggedIn], 
        children: [
            { path: 'client/:id', component: ManagementClient, canActivate: [CanActivateWhenLoggedIn] },
            { path: 'location/:id', component: ManagementLocation, canActivate: [CanActivateWhenLoggedIn] },
            { path: 'project/:id', component: ManagementProject, canActivate: [CanActivateWhenLoggedIn] }
        ]
    }
];