import { Locations } from '../collections/locations.collection';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

Meteor.methods({
    insertLocation: function(location) {
        check(location, Object);

        //check for duplicates
        let possibleDuplicate = Locations.collection.findOne({name: location.name, userId: this.userId});
        
        if(!this.userId) throw new Meteor.Error(404, 'You have to be logged in');
        if(possibleDuplicate) throw new Meteor.Error(404, 'A location already exists with this name');

        return Locations.collection.insert(location);
    },
    updateLocation: function(locationId: string, locationName: string, locationCoordLat: string, locationCoordLong: string) {
        let latitude = parseFloat(locationCoordLat);
        let longitude = parseFloat(locationCoordLong);
        check(locationId, String);
        check(locationName, String);
        check(latitude, Number);
        check(longitude, Number);

        let location = Locations.collection.findOne(locationId);

        if(location && location.name !== locationName) {
            let possibleDuplicate = Locations.collection.findOne({name: locationName, userId: this.userId});

            if(possibleDuplicate) throw new Meteor.Error(404, 'A location already exists with this name');
        }

        if(!location) throw new Meteor.Error(404, 'Location not found');
        if(!this.userId) throw new Meteor.Error(404, 'You have to be logged in');
        if(location.userId !== this.userId) throw new Meteor.Error(404, 'Not authorized');

        return Locations.collection.update(locationId, {$set: {
            name: locationName,
            coord: {
                latitude: latitude,
                longitude: longitude
            },
            updatedAt: new Date()
        }});
    },
    removeLocation: function(locationId: string) {
        check(locationId, String);

        let location = Locations.collection.findOne(locationId);

        if(!location) throw new Meteor.Error(404, 'Location not found');
        if(!this.userId) throw new Meteor.Error(404, 'You have to be logged in');
        if(location.userId !== this.userId) throw new Meteor.Error(404, 'Not authorized');

        return Locations.collection.remove(locationId);
    }
});