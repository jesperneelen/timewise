import { Projects } from '../collections/projects.collection';
import { Project } from '../models/project.model';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

Meteor.methods({
    insertProject: function(project : Project) {
        check(project, Object);

        //check for duplicates
        let possibleDuplicate = Projects.collection.findOne({name: project.name, userId: this.userId});
        let possibleKeyDuplicate = Projects.collection.findOne({key: project.key, userId: this.userId});
        
        if(!this.userId) throw new Meteor.Error(404, 'You have to be logged in');
        if(possibleDuplicate) throw new Meteor.Error(404, 'A project already exists with this name');
        if(possibleKeyDuplicate) throw new Meteor.Error(404, 'Project key is already in use by some other project');

        return Projects.collection.insert(project);
    },
    updateProject: function(projectId: string, projectName: string, projectKey: string, projectDisabled: boolean, projectClientId: string, projectLocationIds: Array<string>, projectTimesheetCodes: Array<string>) {
        check(projectId, String);
        check(projectName, String);
        check(projectKey, String);
        check(projectDisabled, Boolean);
        check(projectClientId, String);
        check(projectLocationIds, Array);
        check(projectTimesheetCodes, Array)

        let project = Projects.collection.findOne(projectId);

        if(project && project.name !== projectName) {
            let possibleDuplicate = Projects.collection.findOne({name: projectName, userId: this.userId});

            if(possibleDuplicate) throw new Meteor.Error(404, 'A project already exists with this name');
        }

        if(project && project.key !== projectKey) {
            let possibleKeyDuplicate = Projects.collection.findOne({key: projectKey, userId: this.userId});
            
            if(possibleKeyDuplicate) throw new Meteor.Error(404, 'Project key is already in use by some other project');
        }

        if(!project) throw new Meteor.Error(404, 'Project not found');
        if(!this.userId) throw new Meteor.Error(404, 'You have to be logged in');
        if(project.userId !== this.userId) throw new Meteor.Error(404, 'Not authorized');

        return Projects.collection.update(projectId, {$set: {
            name: projectName, 
            key: projectKey,
            clientId: projectClientId,
            locationIds: projectLocationIds,
            timesheetCodes: projectTimesheetCodes,
            disabled: projectDisabled, 
            updatedAt: new Date()
        }});
    },
    removeProject: function(projectId) {
        check(projectId, String);

        let project = Projects.collection.findOne(projectId);

        if(!project) throw new Meteor.Error(404, 'Project not found');
        if(!this.userId) throw new Meteor.Error(404, 'You have to be logged in');
        if(project.userId !== this.userId) throw new Meteor.Error(404, 'Not authorized');

        return Projects.collection.remove(projectId);
    }
});