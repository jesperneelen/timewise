import { Clients } from '../collections/clients.collection';
import { Client } from '../models/client.model';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

Meteor.methods({
    insertClient: function(client : Client) {
        check(client, Object);

        //check for duplicates
        let possibleDuplicate = Clients.collection.findOne({name: client.name, userId: this.userId});
        
        if(!this.userId) throw new Meteor.Error(404, 'You have to be logged in');
        if(possibleDuplicate) throw new Meteor.Error(404, 'A client already exists with this name');

        return Clients.collection.insert(client);
    },
    updateClient: function(clientName : string, clientId : string) {
        check(clientName, String);
        check(clientId, String);

        let client = Clients.collection.findOne(clientId);

        //Check for duplicates if name changed while updating
        if(client && clientName !== client.name) {
            let possibleDuplicate = Clients.collection.findOne({name: clientName, userId: this.userId});

            if(possibleDuplicate) throw new Meteor.Error(404, 'A client already exists with this name')
        }

        if(!client) throw new Meteor.Error(404, 'Client not found');
        if(!this.userId) throw new Meteor.Error(404, 'You have to be logged in');
        if(client.userId !== this.userId) throw new Meteor.Error(404, 'Not authorized');

        return Clients.collection.update(clientId, {$set: {
            name: clientName,
            updatedAt: new Date()
        }});
    },
    removeClient: function(clientId : string) {
        check(clientId, String);

        let client = Clients.collection.findOne(clientId);

        if(!client) throw new Meteor.Error(404, 'Client not found');
        if(!this.userId) throw new Meteor.Error(404, 'You have to be logged in');
        if(client.userId !== this.userId) throw new Meteor.Error(404, 'Not authorized');

        return Clients.collection.remove(clientId);
    }
});