import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';

Meteor.methods({
    addUserToRole: (userId) => {
        Roles.addUsersToRoles(userId, ['authenticated-user']);
    },
    updateUserFirstLogin: () => {
        return Meteor.users.update({_id: this.userId}, { $set : { 'profile.firstLogin' : false}});
    },
    updateUser: (user) => {
        return Meteor.users.update({_id: this.userId}, { $set: user });
    },
    setPassword: (password) => {
        return Accounts.setPassword(this.userId, password, { logout: false });
    }
});