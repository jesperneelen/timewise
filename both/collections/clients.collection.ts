import { MongoObservable } from 'meteor-rxjs';
import { Client } from '../models/client.model';

export let Clients = new MongoObservable.Collection<Client>('clients');

Clients.deny({
    insert(userId, doc) {
        return true;
    },
    update(userId, doc, fields, modifier) {
        return true;
    },
    remove(){
        return true;
    }
});