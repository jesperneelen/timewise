import { MongoObservable } from 'meteor-rxjs';
import { Location } from '../models/location.model';

export let Locations = new MongoObservable.Collection<Location>('locations');

Locations.deny({
    insert(userId, doc) {
        return true;
    },
    update(userId, doc, fields, modifier) {
        return true;
    },
    remove() {
        return true;
    }
});