import { MongoObservable } from 'meteor-rxjs';
import { CheckIn } from '../models/checkIn.model';

export let CheckIns = new MongoObservable.Collection<CheckIn>('checkIns');

CheckIns.deny({
    insert: () => {
        return true;
    }, 
    update: () => {
        return true;
    }, 
    remove: () => {
        return true;
    }
});