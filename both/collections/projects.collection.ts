import { MongoObservable } from 'meteor-rxjs';
import { Project } from '../models/project.model';

export let Projects = new MongoObservable.Collection<Project>('projects');

Projects.deny({
    insert(userId, doc) {
        return true;
    },
    update(userId, doc, fields, modifier) {
        return true;
    },
    remove(){
        return true;
    }
});