export interface CheckIn {
    _id?: string, 
    success : boolean,
    time : { start : Date, end? : Date},
    timesheetcode : string,
    userId : string, 
    locationId : string
}