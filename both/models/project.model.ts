export interface Project {
    _id? : string,
    name : string,
    key : string,
    clientId : string,
    locationIds : Array<string>,
    disabled : boolean,
    timesheetCodes : Array<string>,
    userId : string,
    createdAt : Date,
    updatedAt : Date
}